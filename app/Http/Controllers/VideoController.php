<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function index($name){
        return response()->download('videos/'.$name);
    }
}
